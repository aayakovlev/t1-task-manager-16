package ru.t1.aayakovlev.tm.component;

import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.controller.CommandController;
import ru.t1.aayakovlev.tm.controller.ProjectTaskController;
import ru.t1.aayakovlev.tm.controller.TaskController;
import ru.t1.aayakovlev.tm.controller.impl.CommandControllerImpl;
import ru.t1.aayakovlev.tm.controller.impl.ProjectControllerImpl;
import ru.t1.aayakovlev.tm.controller.impl.ProjectTaskControllerImpl;
import ru.t1.aayakovlev.tm.controller.impl.TaskControllerImpl;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.aayakovlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.impl.CommandRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.service.impl.*;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.EMPTY_ARRAY_SIZE;
import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class Bootstrap {

    private static final CommandRepository commandRepository = new CommandRepositoryImpl();

    private static final CommandService commandService = new CommandServiceImpl(commandRepository);

    private static final CommandController commandController = new CommandControllerImpl(commandService);

    private static final TaskRepository taskRepository = new TaskRepositoryImpl();

    private static final TaskService taskService = new TaskServiceImpl(taskRepository);

    private static final TaskController taskController = new TaskControllerImpl(taskService);

    private static final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    private static final ProjectTaskService projectTaskService = new ProjectTaskServiceImpl(projectRepository, taskRepository);

    private static final ProjectTaskController projectTaskController = new ProjectTaskControllerImpl(projectTaskService);

    private static final ProjectService projectService = new ProjectServiceImpl(projectRepository);

    private static final BaseController projectController = new ProjectControllerImpl(projectService, projectTaskService);

    private static final LoggerService loggerService = new LoggerServiceImpl();

    private void initData() {
        projectService.save(new Project("project-1", "description"));
        projectService.save(new Project("project-2", Status.COMPLETED));
        projectService.save(new Project("project-3", Status.IN_PROGRESS));
        projectService.save(new Project("project-4", Status.COMPLETED));

        taskService.save(new Task("task-1", "description"));
        taskService.save(new Task("task-2", Status.IN_PROGRESS));
        taskService.save(new Task("task-3", Status.COMPLETED));
        taskService.save(new Task("task-4", Status.IN_PROGRESS));
        taskService.save(new Task("task-5", Status.COMPLETED));
        taskService.save(new Task("task-6", "description"));
    }

    private void initLogger() {
        loggerService.info(
                "___________              __                                                        \n" +
                        "\\__    ___/____    _____|  | __   _____ _____    ____ _____     ____   ___________ \n" +
                        "  |    |  \\__  \\  /  ___/  |/ /  /     \\\\__  \\  /    \\\\__  \\   / ___\\_/ __ \\_  __ \\\n" +
                        "  |    |   / __ \\_\\___ \\|    <  |  Y Y  \\/ __ \\|   |  \\/ __ \\_/ /_/  >  ___/|  | \\/\n" +
                        "  |____|  (____  /____  >__|_ \\ |__|_|  (____  /___|  (____  /\\___  / \\___  >__|   \n" +
                        "               \\/     \\/     \\/       \\/     \\/     \\/     \\//_____/      \\/       ");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** APPLICATION SHUTTING DOWN ***");
            }
        });
    }

    private boolean processArguments(final String[] arguments) throws ArgumentNotSupportedException {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return false;

        final String argument = arguments[FIRST_ARRAY_ELEMENT_INDEX];
        return processArgument(argument);
    }

    private boolean processArgument(final String argument) throws ArgumentNotSupportedException {
        if (argument == null || argument.isEmpty()) return false;

        switch (argument) {
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            default:
                throw new ArgumentNotSupportedException(argument);
        }
        return true;
    }

    private void processCommand(final String command) throws AbstractException {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConstant.ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.INFO:
                commandController.showInfo();
                break;
            case CommandConstant.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case CommandConstant.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case CommandConstant.PROJECT_CLEAR:
                projectController.clear();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case CommandConstant.PROJECT_CREATE:
                projectController.create();
                break;
            case CommandConstant.PROJECT_LIST:
                projectController.showAll();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case CommandConstant.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case CommandConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case CommandConstant.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case CommandConstant.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case CommandConstant.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConstant.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case CommandConstant.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case CommandConstant.TASK_CLEAR:
                taskController.clear();
                break;
            case CommandConstant.TASK_COMPLETE_BY_ID:
                taskController.completeById();
                break;
            case CommandConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeByIndex();
                break;
            case CommandConstant.TASK_CREATE:
                taskController.create();
                break;
            case CommandConstant.TASK_LIST:
                taskController.showAll();
                break;
            case CommandConstant.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case CommandConstant.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case CommandConstant.TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case CommandConstant.TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case CommandConstant.TASK_SHOW_BY_PROJECT_ID:
                taskController.showByProjectId();
                break;
            case CommandConstant.TASK_START_BY_ID:
                taskController.startById();
                break;
            case CommandConstant.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case CommandConstant.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case CommandConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case CommandConstant.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConstant.EXIT:
                commandController.showExit();
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public void run(final String[] args) throws ArgumentNotSupportedException {
        if (processArguments(args)) return;
        initData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.print("Enter command: ");
                final String command = nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final AbstractException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}

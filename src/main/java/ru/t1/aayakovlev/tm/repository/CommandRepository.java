package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Command;

public interface CommandRepository {

    Command[] getCommands();

}
